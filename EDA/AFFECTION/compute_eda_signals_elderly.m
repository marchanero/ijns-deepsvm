%% coge las se�ales SCR y calcula parametros estadisticos

verbose = 0;

load('ID_CLASS.mat')  % cargo ID de Luz y Clases a las que pertenecen
n_features = 18;  % numero de parametros a calcular
feature_matrix = zeros(length(ID), n_features + 2); % sumo 2 para la clase y la ID original 

for i=1:length(ID)  % indices de ID para identificar las se�ales
    ID(i)
 
  
    clear analysis data
    close all
    load(strcat(int2str(ID(i)), '_AffectionList_.mat'), 'analysis', 'data')
    EDA = data.conductance;
    SCL = analysis.tonicData;
    SCR = analysis.phasicData;
    res = compute_features(SCR);
    res(end+1) = CLASS(i);
    res(end+1) = ID(i);
    feature_matrix(i,:) = res;
    


    % calculamos algunos parametros sobre SCR

    if verbose == 1
        subplot(2,1,1)
        plot(EDA)
        hold on
        plot(SCL, 'r')
        hold off
        subplot(2,1,2)
        plot(SCR, 'r')
        pause
    end

    
    
    
end

elderly_id = find(feature_matrix(:,19) == 1);
elderly_features = feature_matrix(elderly_id,1:18);
elderly_features(:,end) = 1; %a�ado el grupo
save('elderly_features_AFEECTION', 'elderly_features');


   