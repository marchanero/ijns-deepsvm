function [res] = compute_features(fasica)

    %TEMPORALES
    M=mean(fasica);%media
    SD=std(fasica);%Desviaci�n estandar
    MA=max(fasica);%M�ximo
    MI=min(fasica);%Minimo

    DR=MA-MI;%Rango Din�mico Diferencia entre maximo y minimo

    d1=diff(fasica);%primera derivada
    d2=diff(fasica,2);%segunda derivada
    FM=mean(d1); %Media de la primera derivada
    FD=std(d1); %desviaci�n estandar de la primera derivada
    SM=mean(d2); %Media de la segunda derivada
    SSD=std(d2); %desviaci�n estandar de la segunda derivada

    %MORFOLOGICAS
    AL=0; %longitud de arco
    for i=2:length(fasica)
        AL=AL+sqrt(1+(fasica(i)-fasica(i-1))^2);
    end
    AL=AL/i; %-normalizamos-

    IN=0; %integral
    for i=1:length(fasica)
        IN=IN+abs(fasica(i));
    end
    IN=IN/i;%-normalizamos-

    AP=0; %potencia media normalizada
    for i=1:length(fasica)
        AP=AP+(abs(fasica(i)))^2;
    end
    AP=AP/i;

    RM=sqrt(AP); %normalized root mean square

    IL=IN/AL; %Relacion area per�metro (no s� si es asi)

    EL=RM/AL; % Relacion energ�a y per�metro

    SK=skewness(fasica); %asimetr�a (no s� si es asi) (3er momento)

    KU= kurtosis(fasica); %(4 momento)

    MO=moment(fasica,5); % no s� si tiene que ser 5

    


    res(1)=M;
    res(2)=SD;
    res(3)=MA;
    res(4)=MI;
    res(5)=DR;
    res(6)=FM;
    res(7)=FD;
    res(8)=SM;
    res(9)=SSD;
    res(10)=AL;
    res(11)=IN;
    res(12)=AP;
    res(13)=RM;
    res(14)=IL;
    res(15)=EL;
    res(16)=SK;
    res(17)=KU;
    res(18)=MO;




end

