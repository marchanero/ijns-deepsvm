for a=1:145
    
    %Carga de los datos de la componente fasica 
    load(strcat(num2str(a),'_fear.mat'));
    carpeta=strcat(num2str(a),'_fear');
    %Creacion del directorio de todo el dataset
    directorio='Dataset';
    [status, msg, msgID] = mkdir(directorio, carpeta);
    cd(directorio);
    cd(carpeta);
    
    
    for h=1:200
        cadena=strcat(num2str(h),'_seg');
        [status, msg, msgID] = mkdir(cadena);
        cd(cadena);

        %Inicio de los parametros
        %Clase 1 para stress y clase=2 para neutral 
        clase=1;
        %numero de particiones 
        fasica=phasic;
        L=length(fasica);
        fasica(L+1)=fasica(L);
        vector=[];
        vector_sep=[];
        num=1;
        %Normalizacion de todos los vectores.
        L_n=length(fasica);

        %Separacion cada segundo.
        seg_sep=h;

        %10 hz es la tasa de muestreo del dispositivo.
        separacion=seg_sep*10;

        n_sep=L_n/separacion;
        %Son 200 segundos en cada uno de los tramos.
        %Creacion de uno de los directorios de guardado de cada uno de los datos.

        %Creacion del vector de separacion dinamica
        %Cambio en el directorio


        vector_sep(1)=1;
        for i=2:n_sep+1
            
            vector_sep(i)=(i-1)*separacion;
        end

        j=1;
        k=1;
        
        for i=1:length(vector_sep)
            %Inicios del bucle 
            vector=[];
            
            if j<length(vector_sep)
                b=vector_sep(j);
                c=vector_sep(j+1);
                for i=b:c 
                    %bug de espaciado de los vectores mirar 
                    
                    
                    vector(k)=fasica(i);
                    k=k+1;
                end
                
                j=j+1;
                k=1;
            else
                break
            end

            save(strcat('vector_',num2str(j-1),'.mat'), 'vector');
         end 
    cd(strcat('/Users/robertosanchezreolid/Desktop/IJNS-DSVM/SVM/Dataset/Fear/Dataset/',carpeta));
    end
cd('/Users/robertosanchezreolid/Desktop/IJNS-DSVM/SVM/Dataset/Fear');
disp(a)
end


