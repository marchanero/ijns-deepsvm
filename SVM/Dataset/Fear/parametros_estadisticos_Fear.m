%Recorrer los valores de cada una de las carpetas.
directorio='/Users/robertosanchezreolid/Desktop/IJNS-DSVM/SVM/Dataset/Fear/Dataset';
cd(directorio)
%entra en el directorio 
tic 
for m=1:145
    %Crear las iteraciones de cada uno de los bucles 
    
        carpeta=strcat(num2str(m),'_fear');
        cd(carpeta);
        disp(strcat('Iteracion en carpetas: ',num2str(m)));
        %Entrar en cada una de las carpetas de cada archivo para sacar los
        %parametros
        for k=1:200
            subcarpeta=strcat(num2str(k),'_seg');
            cd(subcarpeta);
            ficheros=dir('*.mat');
            a=numel(ficheros);
            %Creacion de la matriz para rellenar cada una de las columnas 
            matriz=[;];
            
            for j=1:a
                %disp(strcat('Carpeta: ',num2str(j)));
                %disp('Calcula los parametros')
                %Calculo de los parametros para cada uno de los segmentos
                %de la se�al 
                vector=[];
               
                
                load(['vector_',num2str(j),'.mat']);
                %disp(strcat('vector_',num2str(j),'.mat'))
                %clase correspondiente al stress 
                clase=1; 
                fasica=vector;
                %TEMPORALES
                M=mean(fasica);%media
                SD=std(fasica);%Desviaci�n estandar
                MA=max(fasica);%M�ximo
                MI=min(fasica);%Minimo

                DR=MA-MI;%Rango Din�mico Diferencia entre maximo y minimo

                d1=diff(fasica);%primera derivada
                d2=diff(fasica,2);%segunda derivada
                FM=mean(d1); %Media de la primera derivada
                FD=std(d1); %desviaci�n estandar de la primera derivada
                SM=mean(d2); %Media de la segunda derivada
                SSD=std(d2); %desviaci�n estandar de la segunda derivada

                %MORFOLOGICAS
                AL=0; %longitud de arco
                for i=2:length(fasica)
                    AL=AL+sqrt(1+(fasica(i)-fasica(i-1))^2);
                end
                AL=AL/i; %-normalizamos-

                IN=0; %integral
                for i=1:length(fasica)
                IN=IN+abs(fasica(i));
                end
                IN=IN/i;%-normalizamos-

                AP=0; %potencia media normalizada
                for i=1:length(fasica)
                AP=AP+(abs(fasica(i)))^2;
                end
                AP=AP/i;

                RM=sqrt(AP); %normalized root mean square

                IL=IN/AL; %Relacion area per�metro (no s� si es asi)

                EL=RM/AL; % Relacion energ�a y per�metro

                SK=skewness(fasica); %asimetr�a (no s� si es asi) (3er momento)

                KU= kurtosis(fasica); %(4 momento)

                MO=moment(fasica,5); % no s� si tiene que ser 5

                %FRECUENCIA

                fft_fasica=fft(fasica);

                F1= bandpower(fft_fasica,10,[0.1 0.2]); %returns the average power in the frequency range

                F2= bandpower(fft_fasica,10,[0.2 0.3]);

                F3= bandpower(fft_fasica,10,[0.3 0.4]);


                matriz(j,1)=M;
                matriz(j,2)=SD;
                matriz(j,3)=MA;
                matriz(j,4)=MI;
                matriz(j,5)=DR;
                matriz(j,6)=FM;
                matriz(j,7)=FD;
                matriz(j,8)=SM;
                matriz(j,9)=SSD;
                matriz(j,10)=AL;
                matriz(j,11)=IN;
                matriz(j,12)=AP;
                matriz(j,13)=RM;
                matriz(j,14)=IL;
                matriz(j,15)=EL;
                matriz(j,16)=SK;
                matriz(j,17)=KU;
                matriz(j,18)=MO;
                matriz(j,19)=F1;
                matriz(j,20)=F2;
                matriz(j,21)=F3;
                matriz(j,22)=clase;
                %the spectral power in bandwidths 0.1 to 0.2 (F1SC), 0.2 to 0.3 (F2SC)
                %and 0.3 to 0.4 (F3SC) Hz are estimated

            

            end 
            %Sale al directorio principal
            
            cd (strcat(directorio,'/',num2str(m),'_fear'));
            save(strcat(num2str(k),'_seg_param','_fear_',num2str(m),'.mat'),'matriz');
            
        end 
       
        cd(directorio)
   
   

       
    
end 
directorio='/Users/robertosanchezreolid/Desktop/IJNS-DSVM/SVM/Dataset/Fear';
cd(directorio);
toc


