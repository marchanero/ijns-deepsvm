load('1_fear.mat');
cadena='1_fear';
directorio='Dataset';
%Inicio de los parametros
%Clase 1 para stress y clase=2 para neutral 
clase=1;
%numero de particiones 
fasica=phasic;
L=length(fasica);
fasica(L+1)=fasica(L);
vector=[];
vector_sep=[];
num=1;
%Normalizacion de todos los vectores.
L_n=length(fasica);

%Separacion cada segundo.
seg_sep=1;

%10 hz es la tasa de muestreo del dispositivo.
separacion=seg_sep*10;

n_sep=L_n/separacion;

%Son 200 segundos en cada uno de los tramos.
%Creacion de uno de los directorios de guardado de cada uno de los datos.
%[status, msg, msgID] = mkdir(directorio,cadena);
%Creacion del vector de separacion dinamica

for i=2:n_sep+1
    vector_sep(1)=1;
    vector_sep(i)=(i-1)*separacion;
end

j=1;
for i=1:length(vector_sep)
%Inicios del bucle 
    vector=[];

 if j<length(vector_sep)
     
    for i=vector_sep(j):vector_sep(j+1)
        fasica(i);
        vector(i)=fasica(i);
    
        
    end
    j=j+1;
    
 else
     break

 end
    
    save(strcat('vector_',num2str(j-1),'.mat'), 'vector');
end 



