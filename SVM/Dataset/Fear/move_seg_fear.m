%Creacion de las carpetas para fear por segundos.
%Crear la estructura de los directorios
directorio='/Users/robertosanchezreolid/Desktop/IJNS-DSVM/SVM/Dataset/Fear';
cd(directorio)
carpeta='Segundos';


[status, msg, msgID] = mkdir(directorio, carpeta);
cd(carpeta);
for j=1:200
    
    direct=strcat('/Users/robertosanchezreolid/Desktop/IJNS-DSVM/SVM/Dataset/Fear/Segundos');
    folder=strcat(num2str(j),'_segundos');
    [status, msg, msgID] = mkdir(direct, folder);
    disp(strcat('Creacion del directorio',num2str(j)));
    
end 
    


%Mover del directorio 
directorio='/Users/robertosanchezreolid/Desktop/IJNS-DSVM/SVM/Dataset/Fear/Dataset';
cd(directorio)

for m=1:145
     carpeta=strcat(num2str(m),'_fear');
     cd(carpeta);
     disp(strcat('Iteracion en carpetas: ',num2str(m)));
     
     ficheros=dir(strcat('*_seg_param','_fear_',num2str(m),'.mat'));
     a=numel(ficheros);
            
     for n=1:a
        
        fichero_mat=strcat(num2str(n),'_seg_param','_fear_',num2str(m),'.mat');
        destino=strcat(direct,'/',num2str(n),'_segundos');
        
        copyfile(fichero_mat,destino);
        
        
         
     end 
     %vuelta al directorio raiz 
     
    cd(directorio)
    
end 

%Vuelta al directorio raiz 
directorio='/Users/robertosanchezreolid/Desktop/IJNS-DSVM/SVM/Dataset/Fear';
cd(directorio);
