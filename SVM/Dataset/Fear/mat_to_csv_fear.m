%Convertir cada uno de los ficheros de tiempo en ficheros csv
%Direccion de los ficheros de destino 
clear;
clc;

directorio='/Users/robertosanchezreolid/Desktop/IJNS-DSVM/SVM/Dataset/Fear/Segundos';
cd (directorio);

%Recorrer cada uno de los directorios 
for i=1:200
    carpeta=strcat(num2str(i),'_segundos');
    cd(carpeta)
    ficheros=dir(strcat('*_seg_param','_fear_*','.mat'));
    a=numel(ficheros);
    
    disp(strcat('Carpeta: ',num2str(i)));
    matriz_dest=[;];
     
    %Extraer cada uno de las matrices y pasarlas a un fichero csv 
    for j=1:a
        %Carga de cada una de los valores 
        matriz=[;];
       
        nombre=strcat(num2str(i),'_seg_param','_fear_',num2str(j),'.mat');
        FileData = load(nombre);
        %Concatenamos las matrices extraidas
        matriz=FileData.matriz;
        matriz_dest = vertcat(matriz_dest,matriz);
        
        
        
    end
    fichero=strcat(nombre,'.csv');
    csvwrite(fichero,matriz_dest);
    %Mover el fichero al directorio de destino
    dest_file='/Users/robertosanchezreolid/Desktop/IJNS-DSVM/SVM/Dataset/Fear/csv_fear';
    movefile(fichero,dest_file);
    
    %Cambio en el directorio
    cd(directorio);
    
    
    
    
end 
cd('/Users/robertosanchezreolid/Desktop/IJNS-DSVM/SVM/Dataset/Fear');


clear;



%FileData = load('FileName.mat');
%csvwrite('FileName.csv', FileData.M);
