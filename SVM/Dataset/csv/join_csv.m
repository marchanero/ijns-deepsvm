%%Script para el merge de los ficheros csv en los distintos dataset 
directorio='/Users/robertosanchezreolid/Desktop/IJNS-DSVM/SVM/Dataset/csv';
cd(directorio);

carpeta1='/Users/robertosanchezreolid/Desktop/IJNS-DSVM/SVM/Dataset/csv/csv_fear';
carpeta2='/Users/robertosanchezreolid/Desktop/IJNS-DSVM/SVM/Dataset/csv/csv_neutral';
destino='/Users/robertosanchezreolid/Desktop/IJNS-DSVM/SVM/Dataset/csv/Dataset_CSV';

for i=1:47
    cd(carpeta1);
    fichero=strcat(num2str(i),'_seg_param_fear_145.mat.csv');
    copyfile(fichero,destino);
end 

for i=1:47
    
    cd(carpeta2);
    fichero2=strcat(num2str(i),'_seg_param_neutral_149.mat.csv');
    copyfile(fichero2,destino);
end

cd (destino);

for i=1:47
    
    fichero=strcat(num2str(i),'_seg_param_fear_145.mat.csv');
    fichero2=strcat(num2str(i),'_seg_param_neutral_149.mat.csv');
    
    csv1 = csvread(fichero);
    csv2 = csvread(fichero2);
    allCsv = [csv1;csv2]; % Concatenate vertically
    
    nombre_csv=strcat(num2str(i),'_seg_dataset.csv');
    
    csvwrite(nombre_csv, allCsv);
    
    delete(fichero);
    delete(fichero2);

end 