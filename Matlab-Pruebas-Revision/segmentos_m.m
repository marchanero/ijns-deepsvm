%% Import data from text file
% Script for importing data from the following text file:
%
%    filename: /Users/robertosanchezreolid/Desktop/IJNS-DSVM/Matlab-Pruebas-Revision/matlab_dataset_csv/10_seg_dataset.csv
%
% Auto-generated by MATLAB on 30-Jan-2020 10:13:38

%% Setup the Import Options and import the data
opts = delimitedTextImportOptions("NumVariables", 22);

% Specify range and delimiter
opts.DataLines = [1, Inf];
opts.Delimiter = ",";

% Specify column names and types
opts.VariableNames = ["M","SD", "MA", "MI","DR", "FM", "FD", "SM", "SSD", "AL", "IN", "AP", "RM", "IL", "EL", "SK", "KU", "MO", "F1", "F2", "F3", "Class"];
opts.VariableTypes = ["double", "double", "double", "double", "double", "double", "double", "double", "double", "double", "double", "double", "double", "double", "double", "double", "double", "double", "double", "double", "double", "double"];

% Specify file level properties
opts.ExtraColumnsRule = "ignore";
opts.EmptyLineRule = "read";

dest="/Users/robertosanchezreolid/Desktop/IJNS-DSVM/Matlab-Pruebas-Revision/matlab_dataset_mat/";


for c=1:47
    % Import the data
    dir="/Users/robertosanchezreolid/Desktop/IJNS-DSVM/Matlab-Pruebas-Revision/matlab_dataset_csv/";
    dir2="_seg_dataset.csv";
    % Bucle for para guardar todos las tablas
    segdataset = readtable(strcat(dir,num2str(c),dir2), opts);
    
    save(strcat(dest,num2str(c),'_dataset.mat'),'segdataset');
   
    
    
    %% Clear temporary variables
    
    clear dir
    clear dir2
   
end

