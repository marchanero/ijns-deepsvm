import os
import sys
import csv
import numpy as np
import pandas as pd

directorio = '/Users/robertosanchezreolid/Desktop/IJNS-DSVM/SVM/SVM/'

##############################################################################################################
# Crea el directorio de destino de cada uno de los ficheros

carpeta = 'dataset_df'

direccion = carpeta + '/'

##############################################################################################################

for i in range(1, 48):
    fichero = direccion+str(i)+'_seg_dataset.csv'
    fichero_dest=direccion+str(i)+'_seg_dataset_df.csv'

    print(fichero)
    print(fichero_dest)

##############################################################################################################

    header = open('header.csv')
    fichero1 = open(fichero)
    fichero_des = open(fichero_dest, 'w')

    csv_head = csv.reader(header)
    csv_read = csv.reader(fichero1)

    columns = ['M', 'SD', 'MA', 'MI', 'DR', 'FM', 'FD', 'SM', 'SSD', 'AL', 'IN', 'AP', 'RM', 'IL',
           'EL', 'SK', 'KU', 'MO', 'F1', 'F2', 'F3', 'Class']

    with open(fichero_dest, 'a') as csv_destino:
        writer = csv.writer(csv_destino)
        writer.writerow(columns)
        #print(columns)

    for row in csv_read:
        #print(row)
        with open(fichero_dest, 'a') as csv_destino:
            writer = csv.writer(csv_destino)
            writer.writerow(row)



